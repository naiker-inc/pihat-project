**Description**
This Project Hat mixs and visualizes analog audio inputs.
The HAT attaches to the Pi, which has access to the input and output signals and a speaker could be plugged into the output of the HAT in order to play the processed input

**Instruction on How to Use**
Connect device to Rasberry Pi. 
Choose an input signal
Adjust knobs to augment signal as desired
Connect output to be played or to be recorded
